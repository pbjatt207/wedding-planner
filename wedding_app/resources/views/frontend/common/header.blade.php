<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from jituchauhan.com/wedding/wedding-new/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Mar 2021 06:29:27 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Are you local weddding vendor provider & looking for wedding vendor website template. Wedding Vendor Responsive Website Template suitable for wedding vendor supplier, wedding agency, wedding company, wedding events etc.. ">
    <meta name="keywords" content="Wedding Vendor, wedding template, wedding website template, events, wedding party, wedding cake, wedding dress, wedding couple, couple, Wedding Venues Website Template">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Wedding Vendor | Find The Best Wedding Vendors</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <!-- Template style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="assets/css/fontello.css">
    <!-- Font used in template -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <!--font awesome icon -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- favicon icon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="collapse" id="searcharea">
        <!-- top search -->
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-primary" type="button">Search</button>
          </span>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 logo">
                    <div class="navbar-brand">
                        <a href="{{route('home')}}"><img src="assets/images/logo.png" alt="Wedding Vendors" class="w-100"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="navigation" id="navigation">
                        <ul>
                            <li class="active"><a href="{{route('home')}}">Home</a>
                            </li>
                            <li><a href="{{route('about')}}">About Us</a>
                            </li>
                            <li><a href="{{route('venues')}}">Venues</a>
                                <ul>
                                    <li><a href="vendor-details.php">Barn</a></li>
                                    <li><a href="vendor-details-tabbed.php">Boutique</a></li>
                                    <li><a href="vendor-profile.php">Exclusive use</a></li>
                                    <li><a href="vendor-profile.php">Garden weddings</a></li>
                                    <li><a href="vendor-profile.php">Hotel</a></li>
                                    <li><a href="vendor-profile.php">Castle</a></li>
                                    <li><a href="vendor-profile.php">Castle</a></li>
                                </ul>

                            </li>
                            <li><a href="{{route('news')}}" title="Home" class="animsition-link">News</a>
                            </li>
                            <li><a href="{{route('wedding')}}">Wedding</a>
                            </li>
                            <li><a href="{{route('contect')}}">Contact us</a>
                            </li>
                        </ul>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
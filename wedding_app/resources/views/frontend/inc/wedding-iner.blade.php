@extends('frontend.index')

@section('content')
    <section class="container">
      <div class="headdig-one">
            <p><a href="#">Home</a> /<a href="wedding.html"> wedding</a> / Kinjal & Raj </p>
      </div>
      <div class="img-banner">
            <img src="{{asset('assets/images/CopyofDJI_0276.webp')}}" alt="...">
            <div class="over-lay-imag">
              <h4>Kinjal</h4>
              <i class="fa fa-heart"></i>
              <h4>Raj</h4>
              <hr>
              <h5>Jaipur</h5>
            </div>
      </div>
    </section>
    <section class="container p-0">
        <div class="heding-main-photo">
            <h2>Photo Gallery</h2>
        </div>
        <div class="back-white-main-phto">
            <div class="col-md-12 p-0 st-tabs main-ul-can">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#topphoto" aria-controls="home" role="tab" data-toggle="tab">Top Photos</a></li>
                    <li role="presentation"><a href="#Allphoto" aria-controls="home" role="tab" data-toggle="tab">All Photo</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="topphoto">
                    <div class="container">
                        <div class="main-home-phot">
                            <div class="row">
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
            
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- <div class="container">
                  <div class="closebtn-main-photo">
                  <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
                  <img id="expandedImg" style="width:100%">
                  <div id="imgtext"></div>
                  </div>
              </div> -->
          </div>
          <div role="tabpanel" class="tab-pane photo-inner-tab" id="Allphoto"> 
            <div class="container">
                <div class="col-md-12 p-0 st-tabs main-ul-can">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#wedding" aria-controls="wedding" role="tab" data-toggle="tab">Wedding(35)</a></li>
                        <li role="presentation"><a href="#Engagement" aria-controls="home" role="tab" data-toggle="tab">Engagement(21)</a></li>
                        <li role="presentation"><a href="#Sangeet" aria-controls="home" role="tab" data-toggle="tab">Sangeet/Cocktail(7)</a></li>
                        <li role="presentation"><a href="#Mehndi" aria-controls="home" role="tab" data-toggle="tab">Mehndi(4)</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="wedding">
                 <div class="container">
                        <div class="main-home-phot">
                            <div class="row">
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                       <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="{{asset('assets/images/358A6841.webp')}}" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>


             <div role="tabpanel" class="tab-pane" id="Engagement"> 
                <div class="container">
                        <div class="main-home-phot">
                            <div class="row">
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

              <div role="tabpanel" class="tab-pane" id="Sangeet"> 
                <div class="container">
                        <div class="main-home-phot">
                            <div class="row">
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="Mehndi"> 
                <div class="container">
                        <div class="main-home-phot">
                            <div class="row">
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4 ">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                                <div class="col-lg-4 pb-4">
                                    <div class="img-colo-imag">
                                        <img src="assets/images/358A6841.webp" class="w-100" alt="Nature" onclick="myFunction(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            </div>
        </div>
        
    </div>
</div>
</section>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Flex Nav Script -->
    <script src="js/jquery.flexnav.js" type="text/javascript"></script>
    <script src="js/navigation.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/header-sticky.js"></script>
    <script src="../../../code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script>
    $(function() {
        $("#weddingdate").datepicker();
    });
    </script>
    <script>
function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}
</script>
@endsection
@extends('frontend.index')

@section('content')

<div class="tp-page-head">
        <!-- page header -->
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 m-auto">
                    <div class="page-header text-center">
                        <h1>tareek</h1>
                        <p>Browse pictures from real weddings shared by couples on WedMeGood. Know latest wedding trends, outfit ideas, vendors chosen by real brides & grooms in different cities and culture.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page header -->
    <section class="nav-bar-one">
        <div class="container">
            <div class="row">
                 <div class=" col-md-2">
                    <select class="form-control from-select">
                        <option>All City</option>
                        <option value="Ahmedabad">Ahmedabad</option>
                        <option value="Gandhinagar">Gandhinagar</option>
                        <option value="Rajkot">Rajkot</option>
                        <option value="Surat">Surat</option>
                         <option>All City</option>
                        <option value="Ahmedabad">Ahmedabad</option>
                        <option value="Gandhinagar">Gandhinagar</option>
                        <option value="Rajkot">Rajkot</option>
                        <option value="Surat">Surat</option>
                        <option value="Vadodara">Vadodara</option>
                    </select>
                </div> 
                <div class=" col-md-2">
                    <select class="form-control from-select">
                        <option>All Cultures</option>
                        <option value="Ahmedabad">Ahmedabad</option>
                        <option value="Gandhinagar">Gandhinagar</option>
                        <option value="Rajkot">Rajkot</option>
                        <option value="Surat">Surat</option>
                         <option value="Gandhinagar">Gandhinagar</option>
                        <option value="Rajkot">Rajkot</option>
                        <option value="Surat">Surat</option>
                        <option value="Vadodara">Vadodara</option>
                    </select>
                </div> 
                <div class=" col-md-2">
                    <select class="form-control from-select">
                        <option>All Themes</option>
                        <option value="Ahmedabad">Ahmedabad</option>
                        <option value="Gandhinagar">Gandhinagar</option>
                        <option value="Rajkot">Rajkot</option>
                        <option value="Surat">Surat</option>
                        <option value="Vadodara">Vadodara</option>
                    </select>
                </div>
                <div class=" col-md-6">
                   <div class="loction-img-main">
                    <input type="text" name="example" class="form-control" list="exampleList" placeholder="Search by vendors, bride/groom">
                      <datalist id="exampleList">
                        <option value="Jodhpur">  
                        <option value="Jaipur">
                        <option value="Ajmer">  
                        <option value="Kota">
                        <option value="Bikaner">  
                        <option value="Jodhpur">
                        <option value="Jaipur">  
                        <option value="Ajmer">
                        <option value="Kota">  
                        <option value="Pali">
                        <option value="Kota">  
                        <option value="Ajmer">
                      </datalist>
                  </div>
                </div>
            </div> 
        </div>
    </section>
    <section class="container">
        <div class="headdig-one">
            <p><a href="index.php">Home</a> / wedding </p>
            <h4>Showing 1322 results in All Cities</h4>
        </div>
        <div class="row">
              @foreach($lists as $item)
         <div class="col-lg-4">
                <div class="card mb-4 card-mainn">
                    <a href="{{route('weddinginer')}}">
                        <div class="img-csrd1">
                          <img src="{{asset('assets/images/RNF_RHEA_AVIK_WEDDINGTEASER-27 (2).webp ')}}"class="card-img-top" alt="...">
                      </div>
                      <div class="d-flex">
                          <div class="img-csrd2">
                              <img src="{{asset('assets/images/RNF_RHEA_AVIK_WEDDINGTEASER-27 (2).webp ')}}"class="card-img-top" alt="...">
                          </div>
                          <div class="img-csrd3">
                              <img src="{{asset('assets/images/RNF_RHEA_AVIK_WEDDINGTEASER-27 (2).webp ')}}"class="card-img-top" alt="...">
                              <div class="overlay"><h5>+ 42 Photos</h5></div>
                          </div>
                      </div>
                      <div class="card-body">
                        <h4 class=" h4 text-center card-title">{{ $item->bride_name }} with {{ $item->groom_name }}</h4>
                        <h6 class="h6 text-center margin-t-20"><i class="fa fa-map-marker margin-r-5"></i>{{ $item->description }}</h6>
                    </div>
                </a>
            </div>
        </div>
         @endforeach
        
        
    </div>
</section>
   @endsection
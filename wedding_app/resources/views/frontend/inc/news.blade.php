@extends('frontend.index')

@section('content')

<div class="tp-page-head">
        <!-- page header -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>News</h1>
                        <p>sdkjdfhjfhdjfhsd</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page header -->
    <div class="tp-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">News</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 content-left">
                    <!-- content left -->
                    <div class="row">
                        @foreach($lists as $item)
                        <div class="col-md-6 post-holder">
                            <div class="well-box">
                                <div class="sticky-sign"><i class="fa fa-bookmark"></i></div>
                                <!-- blog holder -->
                                <div class="post-image">
                                    <a href="#"><img src="assets/images/post-pic-2.jpg" class="w-100" alt=""></a>
                                </div>
                                <h1 class="post-title"><a href="#">{{ $item->title }}</a></h1>
                                <p>{{ $item->des }}</p>
                                <a href="{{route('newsiner')}}" class="btn btn-default">Read More</a> </div>
                        </div>
                        @endforeach
                        <div class="col-md-12 tp-pagination">
                            <ul class="pagination">
                                <li>
                                    <a href="#" aria-label="Previous"> <span aria-hidden="true">Previous</span> </a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li>
                                    <a href="" aria-label="Next"> <span aria-hidden="true">NEXT</span> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.content left -->
                
                <!-- /.right sidebar -->
            </div>
        </div>
    </div>
@endsection
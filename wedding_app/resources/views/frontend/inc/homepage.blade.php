@extends('frontend.index')

@section('content')
<div class="slider-bg">
    <!-- slider start-->
    <div id="slider" class="owl-carousel owl-theme slider">
                             @foreach($lists as $l)
        <div class="item">
            <img src="{{asset('storage/slider/'.$l->image)}}" alt="Wedding couple just married">
              <div class="finder-caption">
                <div class="container m-auto">

                        <h1>{{ $l->title }}</h1>
                        <p>{{ $l->short_description }}</p>
                    </div>
                </div>
      </div>
        @endforeach
    </div>
    <div class="find-section">
        <!-- Find search section-->
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 finder-block">
                   
                    <div class="finderform">
                        <form>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <select class="form-control from-select">
                                        <option>Select City</option>
                                         @foreach($city as $c)
                                        <option value="Ahmedabad">{{ $c->title }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <!-- <select class="form-control">
                                        <option>Select Vendor Category</option>
                                        <option value="Venue">Venue</option>
                                        <option value="Photographer">Photographer</option>
                                        <option value="Cake">Cake</option>
                                        <option value="Dj">Dj</option>
                                        <option value="Florist">Florist</option>
                                        <option value="Videography">Videography</option>
                                        <option value="jewellery">Jewellery</option>
                                        <option value="Gifts">Gifts</option>
                                        <option value="Dresses">Dresses</option>
                                    </select> -->
                                    <input type="text" class="form-control" name="Search" placeholder="Search">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Find Vendors</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.Find search section-->
</div>
<!-- slider end-->
<div class="section-space80">
    <!-- Feature Blog Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title mb60 text-center">
                    <h1>Plan your wedding one step at a time</h1>
                    <p>Simple wedding planning tools to help you stay on track. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- feature center -->
            @foreach($prodect as $p)
            <div class="col-md-3">
                <div class="feature-block feature-center mb30">
                    <!-- feature block -->
                    <div class="feature-icon"> <i class="icon icon-size-60 icon-calendar icon-default"></i></div>
                    <h2>{{$p->title}}</h2>
                    <p>{{$p->description}}</p>
                </div>
            </div>
            @endforeach
            <!-- /.feature block -->
            
        </div>
        <!-- /.feature center -->
        <div class="row">
            <div class="col-md-12 text-center"> <a href="#" class="btn btn-primary">Get Started</a> </div>
        </div>
    </div>
</div>
<!-- Feature Blog End -->
<div class="section-space80 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title mb60 text-center">
                    <h1>Featured Wedding Vendor</h1>
                    <p>Many desktop publishing packages and web page editors now use orem psum as their default model text.</p>
                </div>
            </div>
        </div>
        <div class="row ">
             @foreach($venue as $l)
            <div class="col-md-4">
                <!-- vendor box start-->
                <div class="vendor-box">
                    <div class="vendor-image">
                        <!-- vendor pic -->
                        <a href="#"><img src="{{asset('assets/images/vendor-1.jpg')}}" alt="wedding vendor" class="w-100"></a>
                        <div class="feature-label"></div>
                    </div>
                    <!-- /.vendor pic -->
                    <div class="vendor-detail">
                        <!-- vendor details -->
                        <div class="caption">
                            <!-- caption -->
                            <h2><a href="#" class="title">{{ $l->title }}</a></h2>
                            <p class="location"><i class="fa fa-map-marker"></i>{{ $l->des }}</p>
                        </div>
                        <!-- /.caption -->
                    </div>
                    <!-- vendor details -->
                </div>
            </div>
            @endforeach
            <!-- /.vendor box start-->
           
            <!-- /.vendor box start-->
           
        </div>
    </div>
</div>
<div class="section-space80">
    <!-- top location -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title mb60 text-center">
                    <h1>Top Wedding Locations</h1>
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 location-block">
                <!-- location block -->
                <div class="vendor-image">
                    <a href="#"><img src="{{asset('assets/images/location-pic.jpg')}}" alt="" class="w-100"></a> <a href="#" class="venue-lable"><span class="label label-default">New York City</span></a> </div>
            </div>
            <!-- /.location block -->
            <div class="col-md-4 location-block">
                <!-- location block -->
                <div class="vendor-image">
                    <a href="#"><img src="{{asset('assets/images/location-pic-2.jpg')}}" alt="" class="w-100"></a> <a href="#" class="venue-lable"><span class="label label-default">Sydney</span></a> </div>
            </div>
            <!-- /.location block -->
            <div class="col-md-4 location-block">
                <!-- location block -->
                <div class="vendor-image">
                    <a href="#"><img src="{{asset('assets/images/location-pic-3.jpg')}}" alt="" class="w-100"></a> <a href="#" class="venue-lable"><span class="label label-default">Russia</span></a> </div>
            </div>
            <!-- /.location block -->
            <div class="col-md-8 location-block">
                <!-- location block -->
                <div class="vendor-image">
                    <a href="#"><img src="{{asset('assets/images/location-pic-4.jpg')}}" alt="" class="w-100"></a> <a href="#" class="venue-lable"><span class="label label-default">Germany</span></a> </div>
            </div>
            <!-- /.location block -->
            <div class="col-md-4 location-block">
                <!-- location block -->
                <div class="vendor-image">
                    <a href="#"><img src="{{asset('assets/images/location-pic-5.jpg')}}" alt="" class="w-100"></a> <a href="#" class="venue-lable"><span class="label label-default">Paris</span></a> </div>
            </div>
            <!-- /.location block -->
        </div>
    </div>
</div>
<!-- /.top location -->
<div class="section-space80 bg-light">
    <!-- Testimonial Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title mb60 text-center">
                    <h1>Just Get Married Happy Couple</h1>
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 tp-testimonial">
                <div id="testimonial" class="owl-carousel owl-theme">
                     @foreach($testimonial as $t)
                    <div class="item testimonial-block">
                        <div class="couple-pic"><img src="{{asset('assets/images/couple.jpg')}}" alt="" class="rounded-circle"></div>
                        <div class="feedback-caption">
                            <p>"{{ $t->description }}"</p>
                        </div>
                        <div class="couple-info">
                            <div class="name">{{ $t->title }}</div>
                            <div class="date">Thu, 21st June, 2015</div>
                        </div>
                    </div>
                     @endforeach

                   <!--  <div class="item testimonial-block">
                        <div class="couple-pic"><img src="{{asset('assets/images/couple-2.jpg')}}" alt="" class="rounded-circle"></div>
                        <div class="feedback-caption">
                            <p>"Vestibulum vitae neque urna. Duis ut mauris mi. Sed vehicula vestibulum finias their default model text and a search for lorem ipsum will uncover manym elit posuerenia eget sem."</p>
                        </div>
                        <div class="couple-info">
                            <div class="name">Marry &amp; Leary</div>
                            <div class="date">Thu, 13th July, 2015</div>
                        </div>
                    </div>
                    <div class="item testimonial-block">
                        <div class="couple-pic"><img src="{{asset('assets/images/couple-3.jpg')}}" alt="" class="rounded-circle"></div>
                        <div class="feedback-caption">
                            <p>"Had our wedding on 15th Oct 2015 and have to say Jenny and the team made it a wonderful and enjoyable day were Notting was a problem from the build up to the day."</p>
                        </div>
                        <div class="couple-info">
                            <div class="name">Jhon Doe &amp; Doe Jassica</div>
                            <div class="date">Thu, 21st Aug, 2015</div>
                        </div>
                    </div>
                    <div class="item testimonial-block">
                        <div class="couple-pic"><img src="{{asset('assets/images/couple-4.jpg')}}" alt="" class="rounded-circle"></div>
                        <div class="feedback-caption">
                            <p>"Etiam ut metus nisi. Sed non laoreet nisi tinciin interdum risus felis enjoyable day were Notting was a problem from the build up to the dayvel eleifend milaoreet consectetur."</p>
                        </div>
                        <div class="couple-info">
                            <div class="name">Dave Macwan</div>
                            <div class="date">Thu, 12th Sept, 2015</div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- /. Testimonial Section -->
<!--  <div class="section-space80"> -->
    <!-- Call to action -->
    <!-- <div class="container">
        <div class="row">
            <div class="col-md-6 couple-block">
                <div class="couple-icon"><img src="assets/images/couple.svg" alt=""></div>
                <h2>Are you couple find the venue ?</h2>
                <p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                <a href="#" class="btn btn-primary">Find Vendor</a> </div>
            <div class="col-md-6 vendor-block">
                <div class="vendor-icon"><img src="assets/images/vendor.svg" alt=""></div>
                <h2>Are you wedding vender ?</h2>
                <p>Fusce eget elementum quam, vel tempor justo. Ut imperdiet eget sapien dictum aliquam. Nulla maximus sodales dignissim.</p>
                <a href="#" class="btn btn-primary">Add Your Listing</a></div>
        </div>
    </div>
</div> -->
<!-- /. Call to action -->
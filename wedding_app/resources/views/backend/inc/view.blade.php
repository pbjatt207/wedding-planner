<div id="layoutSidenav_content">
<main>
<section class="container">
    <div class="hedding-view">
        <h2>View</h2>
    </div>
    <div class="menu-v-nav">
        <ul>
            <li><a href="#">All(5)</a></li>
            <li><a href="#">Published(4)</a></li>
            <li><a href="#">Drafts(1)</a></li>
            <li><a href="#">Trash(3)</a></li>

        </ul>
    </div>
    <div class="table-view-main">
         <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                DataTable Example
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Summary</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Summary</th>
                            </tr>
                        </tfoot>
                        <tbody><?php
                            foreach($pages as $index => $page) {
                            ?><tr>
                                <td><?php echo $index + 1 ?>.</td>
                                <td><?php echo $page->title ?></td>
                                <td><?php echo $page->summary ?></td>
                                 <td>Edit</td>
                            </tr><?php
                            }
                            ?></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
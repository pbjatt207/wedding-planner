@extends('backend.index')

@section('content')
<div id="layoutSidenav_content">
<main>
<section class="container">
    <div class="hedding-view">
        <h2>plan View</h2>
    </div>
    <div class="menu-v-nav">
        <ul>
            <li><a href="#">All(5)</a></li>
            <li><a href="#">Published(4)</a></li>
            <li><a href="#">Drafts(1)</a></li>
            <li><a href="#">Trash(3)</a></li>

        </ul>
    </div>
    <div class="table-view-main">
         <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                DataTable Example
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Title</th>
                                <th>summary</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i = 1;
                            @endphp
                             @foreach($lists as $l)
                            <tr>
                                <td>{{ $i++}}</td>
                                <td>{{ $l->title }}</td>
                                <td>{{ $l->description }}</td>
                                 <td>Edit</td>
                                 <td>Delete</td>
                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
   @endsection  
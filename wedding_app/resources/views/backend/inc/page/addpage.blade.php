@extends('backend.index')

@section('content')
<div id="layoutSidenav_content">
<main>
   {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\admin\PageController@store', 'files' => true]) !!}
   <section class="container">
      <div class="row">
       <div class="col-lg-9">
          <div class="heding-page-top">
              <h1>Add</h1>
          </div>
       <form>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
           <h2>Title</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" name="title" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" labl-h2-m col-form-label col-form-label-sm">
              <h2>Description</h2>
          </label>
          <div class="col-sm-10">
          <div class="text-are-main">
            <textarea name="description" class="form-control  form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
           </div>
          </div>
        </div>
        <!-- <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
          <h2>Meta Title</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
              <h2>Meta keywords</h2></label>
          <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
              <h2>Meta Description</h2>
              </label>
          <div class="col-sm-10">
          <div class="text-are-main">
            <textarea type="text" class="form-control  form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
           </div>
          </div>
        </div> -->
        <div class="text-center">
          <div class="btn-send-in">
           <button class="btn btn-lg btn-primary" type="submit" value="Submit">SUBMIT</button>
          </div>
        </div>
      </form>
      </div>
      <div class="col-lg-3">
          <div class="img-side-right">
              <img src="assets/img/couple-3.jpg" class="w-100">
          </div>
          <div class="butt-img">
            <button type="button" class="btn btn-lg btn-outline-secondary">
                 <input type="file" id="myFile" name="filename">
            </button>
        </div>
      </div>
     </div>
   </section>
   {!! Form::close() !!}
</main>
         @endsection    
@extends('backend.index')

@section('content')
<div id="layoutSidenav_content">
<main>
  {!! Form::open(['method' => 'POST', 'action' => 'App\Http\Controllers\admin\SettingController@store', 'files' => true]) !!}
   <section class="container">
      <div class="row">
       <div class="col-lg-12">
          <div class="heding-page-top">
              <h1>Edit Contact</h1>
          </div>
       <form>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
           <h2>Location</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" name="location" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>

        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
           <h2>E-mail</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" name="email" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
       
       <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
           <h2>Phone No.</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" name="number" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
       
        <!-- <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
          <h2>Meta Title</h2>
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
              <h2>Meta keywords</h2></label>
          <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
          </div>
        </div>
        <div class="form-main-upps d-flex">
          <label for="colFormLabelSm" class=" col-form-label col-form-label-sm labl-h2-m">
              <h2>Meta Description</h2>
              </label>
          <div class="col-sm-10">
          <div class="text-are-main">
            <textarea type="text" class="form-control  form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
           </div>
          </div>
        </div> -->
        <div class="text-center">
          <div class="btn-send-in">
           <button class="btn btn-lg btn-primary" type="submit" value="Submit">SUBMIT</button>
          </div>
        </div>
      </form>
      </div>
     </div>
   </section>
  {!! Form::close() !!}
</main>
         @endsection    
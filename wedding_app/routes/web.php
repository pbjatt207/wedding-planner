<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('run-cmd/{cmd}', function ($cmd) {
    return \Artisan::call($cmd);
});


Route::group(['namespace' => 'App\Http\Controllers'], function() {
	Route::group(['namespace' => 'admin', 'prefix' => 'tm-admin'], function() {
	 Route::get('/', 'DashboardController@dashboard')->name('dashboard');
	 Route::get('/login', 'AuthController@login')->name('login');
	 Route::resources([
	 	'page' => 'PageController',
	 	'news' => 'NewsController',
	    'venue' => 'VenueController',
	    'wedding'=> 'WeddingController',
	    'slider'=> 'SliderController',
	    'city'=> 'CityController',
	    'products'=> 'ProductsController',
	    'testimonial'=> 'TestimonialController',
	    'setting'=> 'SettingController',

	 ]);
	 Route::get('/contact', 'ContactController@index')->name('contact.index');

	});
	Route::get('/', 'HomeController@home')->name('home');
	Route::get('/about', 'AboutController@about')->name('about');
	Route::get('/contact', 'ContactController@index')->name('contect');
	Route::post('/contact', 'ContactController@store')->name('contect.post');
	Route::get('/Venues', 'VenuesController@venue')->name('venues');
	Route::get('/wedding', 'WeddingController@wedding')->name('wedding');
	Route::get('/news', 'NewsController@news')->name('news');
	Route::get('/newsiner', 'NewsinerController@newsiner')->name('newsiner');
	Route::get('/weddinginer', 'WeddinginerController@weddinginer')->name('weddinginer');
	
});



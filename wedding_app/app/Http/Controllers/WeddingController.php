<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wedding;


class WeddingController extends Controller
{
     public function wedding()
    {
    	$lists = Wedding::all();
    	$data = compact('lists');
        return view('frontend.inc.wedding',$data);
    }
}

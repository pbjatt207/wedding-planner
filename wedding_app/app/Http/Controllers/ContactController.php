<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Http\Request;

class ContactController extends Controller
{

public function index()
    {
        $lists = Setting::first();
        $data = compact('lists');
        return view('frontend.inc.contact-us', $data);
    }
/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'number' => 'required',
        'decription' => 'required',

        ]);

        $store = $request->all();
        // dd($store);
        // dd($store);
        $data = new Contact($store);
        $data->save();
        
        
        return redirect( route('contect') )->with('success', 'Success! New record has been added.');
    }
}

<?php

namespace App\Http\Controllers\admin;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

use Intervention\Image\ImageManagerStatic as Image;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Slider::all();

         return view('backend.inc.slider.view', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('backend.inc.slider.addslider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required',
            'short_description' => 'required',
        ]);

        // $store = $request->all();
        $slider = new Slider($request->all());
        $slider->save();

        $dir = public_path("storage/slider/");
        if (!is_dir($dir)) {
            mkdir($dir, 755, true);
        }

        if($request->hasFile('image')) {

            $image       = $request->file('image');
            $filename    = $slider->id . "." . $image->getClientOriginalExtension();

            $canvas = Image::canvas(1600,900);
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1600, 900, function($constraint) {
                $constraint->aspectRatio();
            });

            $canvas->insert($image_resize, 'center');
            $canvas->save($dir . $filename);

            $slider->image = $filename;
            $slider->save();
        }
        
        
        return redirect( route('slider.index') )->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        //
    }
}

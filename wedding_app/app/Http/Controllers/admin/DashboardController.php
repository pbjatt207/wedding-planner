<?php

namespace App\Http\Controllers\admin;

use App\Models\Home;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
     public function dashboard()
    {
        return view('backend.inc.home');
    }
}

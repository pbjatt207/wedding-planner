<?php

namespace App\Http\Controllers;

use App\Models\Home;
use Illuminate\Http\Request;

class AboutController extends Controller
{
     public function about()
    {
        return view('frontend.inc.about');
    }
}

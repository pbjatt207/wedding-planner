<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venue;

class VenuesController extends Controller
{
     public function venue()
    {
    	$lists = Venue::all();
    	// dd($lists);
    	$data = compact('lists');
        return view('frontend.inc.venue-listing', $data);
    }
}
